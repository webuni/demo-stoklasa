Minimální distribuce Symfony aplikace
=====================================

.. highlight:: bash

Instalace
---------

1. Nastavit oprávnění pro zápis do adresářů::

      chmod a+rwx var/cache/ var/logs var/sessions

2. Spustit a během instalace zadat správné údaje::

      php composer.phar install

3. Během instalace budeme dotázání na nastavení.

Rozdíly distribuce oproti Symfony
---------------------------------

**Konfigurace**
    Každý balíček má konfiguraci v samostatném souboru ``app/config/bundles/<bundle>.yml``.
    Tyto konfigurace se načítají v souboru ``app/config/config.yml``. Konfigurace pro prostředí je
    v souboru ``app/config/environments/<environment>.yml``.

    Pro nastavení administrace slouží konfigurace ``app/config/admin.yml``, zabezpečení se nastavuje
    v ``app/config/security.yml`` a aplikační služby v ``app/config/services.yml``.

**Šablony**
    Veškeré šablony jsou ukládány v adresáři ``app/Resources/views``.

**Chybové stránky**
    Chybové stránky se ve vývojovém módu zobrazují tak, jak budou vidět v produkčním módu. Pouze se navíc
    zobrazuje tab s podrobným popisem výjimky.
