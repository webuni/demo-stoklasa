FROM alpine

RUN apk add --update \
        php-cli \
        php-json \
        php-ctype \
        php-posix \
        php-intl \
        php-dom \
        php-pdo_sqlite \
    && rm -rf /var/cache/apk/*

VOLUME /app
WORKDIR /app

ENTRYPOINT ["/app/bin/console"]
CMD ["server:run", "0.0.0.0:80", "-vvv"]

EXPOSE 80
