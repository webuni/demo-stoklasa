<?php

namespace AppBundle\Twig;

class AppExtension extends \Twig_Extension
{
    public function __construct()
    {
    }

    public function getGlobals()
    {
        // {{ constant('Namespace\\Classname::CONSTANT_NAME') }}
        // twig v zakladu obsahuje globalni promenou app
        return array();
    }

    public function getNodeVisitors()
    {
        return parent::getNodeVisitors();
    }

    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('phpize', array($this, 'phpize'), array('is_safe' => array('html'))),
            new \Twig_SimpleFilter('camelize', array($this, 'camelize'), array('is_safe' => array('html'))),
        );
    }

    public function phpize($property)
    {
        return preg_replace('/([a-z]+)([A-Z])/', "\\1_\\2", $property);
    }

    public function camelize($property)
    {
        return preg_replace(array('/(_)+(.)/e'), array("strtoupper('\\2')"), $property);
    }

    // the name of the Twig extension must be unique in the application. Consider
    // using 'app.extension' if you only have one Twig extension in your application.
    public function getName()
    {
        return 'app.extension';
    }
}
