<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;
use AppBundle\Entity\Article;

/**
 * Doctrine repository obsahuje metody, ktere jsou uzitecne a casto pouzivane pri dotazovani
 * na informace o entite Article
 * See http://symfony.com/doc/current/book/doctrine.html#custom-repository-classes
 */
class ArticleRepository extends EntityRepository
{
    public function findLatest($limit = Article::NUM_ITEMS)
    {
        return $this
            ->createQueryBuilder('p')
            ->select('p')
            ->where('p.publishedAt <= :now')->setParameter('now', new \DateTime())
            ->orderBy('p.publishedAt', 'DESC')
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult()
        ;
    }
}
