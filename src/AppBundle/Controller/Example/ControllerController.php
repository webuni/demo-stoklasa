<?php

namespace AppBundle\Controller\Example;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Description of ControllerController
 *
 * @Route("/ex/controller")
 *
 * @author Petr Jaša
 * @package AppBundle\Controller
 */
class ControllerController extends Controller
{
    /**
     * @Route("/index")
     * @Template("Example/Controller/index.html.twig")
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        // TODO
        // ukazat request, response, session
        // ukazat container
        // ukazat tenky a tlusty kontroler
        // ukazat @Route, @Template, @Security, @ParamConverter, @Cache
        // ukazat event dispatcher, middleware (http://stackphp.com/), AOP (https://github.com/schmittjoh/JMSAopBundle)
        // probrat životní cyklus

        //return new RedirectResponse($this->get('router')->generate('homepage'));

        //return new Response($this->renderView('Example/Controller/index.html.twig', array()));
        //return $this->render('Example/Controller/index.html.twig', array());
        return array(
            'date' => new \DateTime()
        );
    }

    /**
     * @Route("/cache")
     * @Template("Example/Controller/cache.html.twig")
     *
     * @param Request $request
     * @return Response
     * @see http://symfony.com/doc/current/book/http_cache.html
     * @see http://symfony.com/doc/current/bundles/SensioFrameworkExtraBundle/annotations/cache.html
     * @see https://github.com/FriendsOfSymfony/FOSHttpCacheBundle
     */
    public function cacheAction(Request $request)
    {
        // reverse proxy (browser cache - proxy cache - gateway cache)
        // cache headers (Cache-Control, Expires, ETag, Last-Modified)
        // expiration vs validation
        // ESI

        // Co je mozne cachovat? GET HEAD
        // Co je mozne cachovat ve sdilene cache? Neosobni obsah, NE informace o uctu

        $response = new Response($this->renderView('Example/Controller/cache.html.twig', array(
            'date' => new \DateTime()
        )));


        // Cache-Control: private, max-age=0, must-revalidate
        // -------------

        // povolit/zakazat cachovani na sdilenych cache serverech (proxy cache, gateway cache)
        $response->setPrivate();
        $response->setPublic();

        // nastaveni max age max age pro sdilene nebo soukrome cache servery
        // expiration model
        $response->setMaxAge(90);
        $response->setSharedMaxAge(90);

        // Cache-Control directive
        $response->headers->addCacheControlDirective('must-revalidate', true);

        // Vary
        // ----

        // cachovani obsahu na zaklade pozadavku kodovani odpovedi a prohlizece, pro ktery je odpoved urcena
        //$response->setVary(array('Accept-Encoding', 'User-Agent'));

        return $response;
    }

    /**
     * @Route("/cache-expiration")
     * @Template("Example/Controller/cache.html.twig")
     *
     * @param Request $request
     * @return Response
     * @see http://symfony.com/doc/current/book/http_cache.html
     * @see http://symfony.com/doc/current/bundles/SensioFrameworkExtraBundle/annotations/cache.html
     * @see https://github.com/FriendsOfSymfony/FOSHttpCacheBundle
     */
    public function cacheExpirationAction(Request $request)
    {
        $response = new Response($this->renderView('Example/Controller/cache.html.twig', array(
            'date' => new \DateTime()
        )));

        // Expiration model
        // ----------------

        $date = new \DateTime();
        $date->modify('+120 seconds');
        $response->setPublic();
        $response->setExpires($date); // converted to GMT

        return $response;
    }

    /**
     * @Route("/cache-validation")
     * @Template("Example/Controller/cache.html.twig")
     *
     * @param Request $request
     * @return Response
     * @see http://symfony.com/doc/current/book/http_cache.html
     * @see http://symfony.com/doc/current/bundles/SensioFrameworkExtraBundle/annotations/cache.html
     * @see https://github.com/FriendsOfSymfony/FOSHttpCacheBundle
     */
    public function cacheValidationAction(Request $request)
    {
        $response = new Response($this->renderView('Example/Controller/cache.html.twig', array(
            'date' => new \DateTime()
        )));

        // Validation model
        // ----------------

        // etag
        //$response->setETag(md5($response->getContent()));
        $response->setPublic();
        // If-None-Match header z Requestu s ETag hodnotou vytvarene odpovedi,
        // nastavi http status kod 304, pokud je obsah stejny
        $response->isNotModified($request);

        // last modified
        //$response->setLastModified(new \DateTime('2015-02-01'));
        $response->setPublic();
        // If-Modified-Since header z Requestu s Last-Modified hodnotou vytvarene odpovedi,
        // nastavi http status kod 304, pokud je cas posledni modifikace stejny
        $response->isNotModified($request);

        return $response;
    }

    /**
     * @see http://www.w3.org/TR/esi-lang
     */
    public function esiAction()
    {
        $date = new \DateTime();
        $response = new Response($date->format('H:i:s'));
        $response->setSharedMaxAge(60);

        return $response;
    }

    /**
     * @return Response
     */
    public function hincludeAction()
    {
        sleep(10);

        $date = new \DateTime();
        $response = new Response("hinclude: " . $date->format('H:i:s'));

        return $response;
    }
}
