<?php

namespace AppBundle\Controller\Example;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Description of RoutingController
 *
 * @Route("/ex/routing")
 *
 * @author Petr Jaša
 * @package AppBundle\Controller
 */
class RoutingController extends Controller
{
    public function helloAction($name)
    {
        // TODO
        // ukazat front controller
        // ukazat configuraci framework.yml "framework:router"
        // ukazat configuraci routing.yml (definice rout)
        // ukazat jak se generuji url adresy v kontroleru a v sablone
        // ukazat https://github.com/FriendsOfSymfony/FOSJsRoutingBundle

        //dump($request->attributes);

        if ($name == 'Petr') {
            return $this->forward('AppBundle:Example/Routing:hello', array('name' => 'Martin'));
        }

        if ($name == 'Seznam') {
            return $this->redirect('http://seznam.cz');
        }

        if ($name == 'Home') {
            return $this->redirect($this->generateUrl('homepage', array(), false));
        }

        if ($name == 'Homepage') {
            return $this->redirectToRoute('homepage');
        }

        return $this->render('Example/Routing/hello.html.twig', array(
            "name" => $name
        ));
    }

    /**
     * @Route("/goodbye/{name}")
     *
     * @param $name
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function goodbyeAction($name)
    {
        return $this->render('Example/Routing/goodbye.html.twig', array(
            "name" => $name
        ));
    }
}
