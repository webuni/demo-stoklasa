<?php

namespace AppBundle\Controller\Example;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Description of TwigController
 *
 * @Route("/ex/twig")
 *
 * @author Petr Jaša
 * @package AppBundle\Controller
 */
class TwigController extends Controller
{
    /**
     * @Route("/index")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        // TODO
        // ukazat configuraci framework.yml "framework:templating"
        // ukazat configuraci twig.yml
        // ukazat dědičnost a sestavování šablon:
        //     - extends, include, embed, use, macro, import
        //     - render(controller)
        // ukazat escapovani, kontrola prazdnych znaku
        // ukazat for, if
        // ukazat twig extensions
        // ukazat jak zapnout haml, twital
        // ukazat rozsireni, ktere ukazuje jaka sablona byla pouzita pro vykresleni casti stranky

        return $this->get('templating')->renderResponse("Example/Twig/index.html.twig", array());
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function renderAction()
    {
        return $this->get('templating')->renderResponse("Example/Twig/controller.html.twig", array(
            'greeting' => 'Toto je text z šablony controller.html.twig'
        ));
    }
}
