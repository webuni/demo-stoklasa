<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Controller used to manage blog contents in the public part of the site.
 *
 * @Route("/blog")
 *
 * @author Ryan Weaver <weaverryan@gmail.com>
 * @author Javier Eguiluz <javier.eguiluz@gmail.com>
 */
class BlogController extends Controller
{
    /**
     * @Route("/", name="blog_index")
     * @Method("GET")
     * @Template("Blog/index.html.twig")
     */
    public function indexAction()
    {
        //$em = $this->getDoctrine()->getManager();
        //$aticles = $em->getRepository('AppBundle:Article')->findLatest();

        $articles = $this->get('article_repository')->findLatest();

        return array(
            'articles' => $articles
        );
    }
}
