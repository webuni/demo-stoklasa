<?php

/*
 * This file is part of the Symfony Minimal Edition package.
 *
 * (c) Webuni s.r.o.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Templating\TemplateReference;
use Symfony\Bundle\TwigBundle\Controller\ExceptionController as BaseExceptionController;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

class ExceptionController extends BaseExceptionController implements ContainerAwareInterface
{
    public function __construct()
    {
    }

    public function setContainer(ContainerInterface $container = null)
    {
        $this->twig = $container->get('twig');
        $this->debug = $container->getParameter('kernel.debug');
    }

    protected function findTemplate(Request $request, $format, $code, $showException)
    {
        $template = new TemplateReference(null, null, 'Exception/'.($showException ? 'exception' : 'error'), $format, 'twig');
        if ($this->templateExists($template)) {
            return $template;
        } else {
            return parent::findTemplate($request, $format, $code, $showException);
        }
    }
}
