<?php

namespace AppBundle\Controller\Admin;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Description of BlogController
 *
 * @Route("%app.admin_url_prefix%/post")
 *
 * @author Petr Jaša
 * @package AppBundle\Controller\Admin
 */
class BlogController extends Controller
{
    /**
     * @Route("/", name="admin_index")
     * @Route("/", name="admin_post_index")
     * @Method("GET")
     * @Template("Admin/Blog/index.html.twig")
     */
    public function indexAction()
    {
        //$em = $this->getDoctrine()->getManager();
        //$aticles = $em->getRepository('AppBundle:Article')->findLatest();

        $articles = $this->get('article_repository')->findLatest();

        return array(
            'articles' => $articles
        );
    }
}
